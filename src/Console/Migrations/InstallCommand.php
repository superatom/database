<?php namespace Superatom\Console\Migrations;

use Illuminate\Database\Migrations\MigrationRepositoryInterface;
use Superatom\Console\Command;

class InstallCommand extends Command
{
    /**
     * @var MigrationRepositoryInterface
     */
    protected $repository;

    public function __construct(MigrationRepositoryInterface $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }
    public function handle()
    {
        $this->repository->createRepository();

        $this->info('Migration table created successfully.');
    }

    protected function configure()
    {
        $this
            ->setName('migrate:install')
            ->setDescription('Create the migration repository')
        ;
    }
}