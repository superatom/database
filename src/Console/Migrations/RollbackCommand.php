<?php namespace Superatom\Console\Migrations;

use Superatom\Console\Command;
use Superatom\Database\Migrator;

class RollbackCommand extends Command
{
    /**
     * @var Migrator
     */
    protected $migrator;

    public function __construct(Migrator $migrator)
    {
        parent::__construct();

        $this->migrator = $migrator;
    }

    public function handle()
    {
        $pretend = $this->option('pretend');

        $this->migrator->rollback($pretend);

        foreach ($this->migrator->getNotes() as $note) {
            $this->line($note);
        }
    }

    protected function configure()
    {
        $this
            ->setName('migrate:rollback')
            ->setDescription('Rollback the last database migration')
            ->addBoolOption('pretend', null, 'Dump the SQL queries that would be run')
        ;
    }
}