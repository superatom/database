<?php namespace Superatom\Console\Migrations;

use Illuminate\Database\Migrations\Migrator;
use Superatom\Console\Command;
use Symfony\Component\Console\Input\ArrayInput;

class MigrateCommand extends Command
{
    /**
     * @var Migrator
     */
    protected $migrator;

    /**
     * Path to migration files
     *
     * @var string
     */
    protected $migrationPath;

    public function __construct(Migrator $migrator, $migrationPath)
    {
        parent::__construct();

        $this->migrator = $migrator;
        $this->migrationPath = $migrationPath;
    }

    public function handle()
    {
        $this->prepareDatabase();

        $this->migrator->run($this->migrationPath, $this->option('pretend'));

        foreach ($this->migrator->getNotes() as $note) {
            $this->line($note);
        }

        // TODO: run seed command
    }

    protected function configure()
    {
        $this
            ->setName('migrate')
            ->setDescription('Run the database migrations')
            ->addBoolOption('pretend', null, 'Dump the SQL queries that would be run');
        ;
    }

    /**
     * Prepare the migration database
     */
    protected function prepareDatabase()
    {
        if ($this->migrator->repositoryExists()) {
            return;
        }

        $this->call('migrate:install');
    }
}