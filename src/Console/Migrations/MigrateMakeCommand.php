<?php namespace Superatom\Console\Migrations;

use Superatom\Console\Commands\GeneratorCommand;

class MigrateMakeCommand extends GeneratorCommand
{
    public function __construct($migrationPath)
    {
        parent::__construct($migrationPath);
    }

    public function handle()
    {
        $name = $this->argument('name');
        $table = $this->option('table');

        $replacement = [];
        $replacement['DummyClass'] = studly_case($name);
        if ($table) {
            $replacement['DummyTable'] = $table;
        }

        $filename = date('Y_m_d_His') . '_' . $name . '.php';

        $this->generate($filename, $replacement);

        $this->info('Created migration: ' . $filename);

        $this->getApplication()->getComposer()->dumpAutoloads();
    }

    /**
     * get stub text
     *
     * @return string
     */
    public function getStub()
    {
        $path = __DIR__ . '/stubs';

        if (is_null($this->option('table'))) {
            return $this->getApplication()->getFilesystem()->get($path . '/blank.stub');
        }

        $stub = $this->option('create') ? '/create.stub' : '/update.stub';

        return $this->getApplication()->getFilesystem()->get($path . $stub);
    }

    protected function configure()
    {
        $this
            ->setName('make:migrate')
            ->setDescription('Create a new migration file')
            ->addArgument('name', 'The name of the migration')
            ->addBoolOption('create', null, 'The table to be created')
            ->addOption('table', null, 'The table to migrate')
        ;
    }
}