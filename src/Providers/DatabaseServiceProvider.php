<?php namespace Superatom\Providers;

use Illuminate\Events\Dispatcher;
use Pimple\Container as Pimple;
use Pimple\ServiceProviderInterface;
use Superatom\Database\ConnectionResolver;
use Illuminate\Container\Container;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\Eloquent\Model;

class DatabaseServiceProvider implements ServiceProviderInterface
{
    public function register(Pimple $app)
    {
        $container = new Container;
        $factory = new ConnectionFactory($container);
        $resolver = new ConnectionResolver($app['config']->get('database'), $factory);

        Model::setConnectionResolver($resolver);
        Model::setEventDispatcher(new Dispatcher($container));

        $app['db'] = $resolver;
    }
}