<?php namespace Superatom\Providers;

use Illuminate\Session\DatabaseSessionHandler;
use Illuminate\Session\Store;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class DatabaseSessionServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['session'] = function() use ($app)
        {
            $config = $app['config']->get('session');
            $handler = new DatabaseSessionHandler($app['db']->connection(), $config['table']);

            return new Store($config['cookie'], $handler);
        };
    }
}