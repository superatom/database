<?php namespace Superatom\Database;

class Migration extends \Illuminate\Database\Migrations\Migration
{
    /**
     * @var \Illuminate\Database\Schema\Builder
     */
    protected static $schema;

    /**
     * @param \Illuminate\Database\Schema\Builder $schema
     */
    public static function setSchemaOnce($schema)
    {
        if (is_null(static::$schema)) static::$schema = $schema;
    }
}