<?php namespace Superatom\Database;

class Migrator extends \Illuminate\Database\Migrations\Migrator
{
    public function resolve($file)
    {
        /* @var $class \Superatom\Database\Migration */
        $class = parent::resolve($file);
        $class::setSchemaOnce($this->resolveConnection(null)->getSchemaBuilder());

        return $class;
    }
}