<?php namespace Superatom\Database;

use Illuminate\Database\ConnectionResolverInterface;
use Illuminate\Database\Connectors\ConnectionFactory;

class ConnectionResolver implements ConnectionResolverInterface
{
    /**
     * Database configurations
     *
     * @var array
     */
    protected $config;

    /**
     * ConnectionFactory instance
     *
     * @var ConnectionFactory
     */
    protected $factory;

    /**
     * Connection instances
     *
     * @var array
     */
    protected $connections;

    /**
     * @param array $config
     * @param ConnectionFactory $factory
     */
    public function __construct($config, $factory)
    {
        $this->config = $config;
        $this->factory = $factory;
    }

    /**
     * Get a database connection instance.
     *
     * @param  string $name
     * @return \Illuminate\Database\Connection
     */
    public function connection($name = null)
    {
        $name = $name ?: $this->getDefaultConnection();

        if ( ! isset($this->connections[$name])) {
            $config = $this->getConfig($name);
            $this->connections[$name] = $this->factory->make($config);
        }

        return $this->connections[$name];
    }

    /**
     * Get the default connection name.
     *
     * @return string
     */
    public function getDefaultConnection()
    {
        return $this->config['default'];
    }

    /**
     * Set the default connection name.
     *
     * @param  string $name
     * @return void
     */
    public function setDefaultConnection($name)
    {
        $this->config['default'] = $name;
    }

    /**
     * @param string $name
     * @return array
     */
    protected function getConfig($name)
    {
        $config = $this->config['connections'];

        if ( ! isset($config[$name])) {
            throw new \InvalidArgumentException("Database [$name] not configured.");
        }

        return $config[$name];
    }
}